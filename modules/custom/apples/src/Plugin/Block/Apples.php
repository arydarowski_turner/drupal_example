<?php
/**
 * Provides an 'Apples' block.
 *
 * @Block(
 *   id = "apples",
 *   admin_label = @Translation("Apples"),
 *   category = @Translation("Aimee")
 * )
 */

namespace Drupal\apples\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

class Apples extends BlockBase {
  /**
   *  {@inheritdoc}
   */
  public function build()
  {
    $config = $this->getConfiguration();
    if (!empty($config['apples_quantity'])) {
      $quantity = intval($config['apples_quantity']);
    }
    else {
      $quantity = 'no';
    }
    return array(
      '#markup' => $this->t('I have '. $quantity . ' apples.'),
    );
  }

  /**
   *  {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);

    $config =  $this->getConfiguration();

    $form['apples_quantity'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('How Many'),
      '#description' => $this->t('How many apples would you like?'),
      '#default_value' => isset($config['apples_quantity']) ?
        $config['apples_quantity'] : ''
    );

    return $form;
  }

  /**
   *  {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['apples_quantity'] =
      $form_state->getValue('apples_quantity');
  }

  /**
   *  @{inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state)
  {
    $quantity = $form_state->getValue('apples_quantity');

    if(!is_numeric($quantity)) {
      $error_message = 'Quantity needs to be an integer.';
      $form_state->setErrorByName('apples_quantity', $error_message);
      drupal_set_message(t($error_message), 'error');
    }
  }


}

<?php
/**
 * @file
 * Contains \Drupal\gridster_test\Controller\GridsterTest.
 *
 */

namespace Drupal\gridster_test\Controller;

use Drupal\Core\Controller\ControllerBase;

class GridsterTest extends ControllerBase
{
  /**
   * {@inheritdoc}
   */
  public function content()
  {
    return array(
      '#theme' => 'gridster_test',
      '#attached' => array(
        'library' =>  array(
          'gridster_test/gridster',
         )
       ),
      '#var1' => 'This is just a test.',
      '#var2' => $this->myFirstFunction(),
      '#var3' => 'Nothing to see here.',
    );
  }

  private function myFirstFunction()
  {
    $message = 'Well hello...';
    return $message;
  }

}



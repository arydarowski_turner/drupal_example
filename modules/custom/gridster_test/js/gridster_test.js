var gridster;
var html;

(function ($) {
  $(document).ready(function() {
    gridster =$(".gridster ul").gridster({
        widget_margins: [10, 10],
        widget_base_dimensions: [140, 140],
        max_cols: 6,
    }).data('gridster');

    $("#js-serialize").click(function() {
        var s = gridster.serialize();
        $('#log').val(JSON.stringify(s));
    });
    console.log(gridster);
    $("#add-widget").click(function() {
        //$this.add_widget( html, [size_x], [size_y], [col], [row] )
        html='<li class="pink" data-row="1" data-col="1" data-sizex="2" data-sizey="1">PINK</li>';
        gridster.add_widget(html, 2, 1, 1, 1);
        gridster.cols = 7;
        console.log(gridster);

    });
});
})(jQuery);

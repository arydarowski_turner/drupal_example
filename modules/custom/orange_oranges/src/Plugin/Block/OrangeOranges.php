<?php
/**
 * Provides an 'Orange Oranges' block.
 *
 * @Block(
 *   id = "oranges_oranges",
 *   admin_label = @Translation("Orange_Oranges"),
 *   category = @Translation("Aimee")
 * )
 */

namespace Drupal\orange_oranges\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

class OrangeOranges extends BlockBase {
  /**
   *  {@inheritdoc}
   */
  public function build()
  {
    $config = $this->getConfiguration();
    if (!empty($config['oranges_quantity'])) {
      $quantity = intval($config['oranges_quantity']);
    }
    else {
      $quantity = 'no';
    }
    return array(
      '#markup' => $this->t('I have '. $quantity . ' oranges.'),
    );
  }

  /**
   *  {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);

    $config =  $this->getConfiguration();

    $form['oranges_quantity'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('How Many'),
      '#description' => $this->t('How many oranges would you like?'),
      '#default_value' => isset($config['oranges_quantity']) ? $config['oranges_quantity'] : ''
    );

    return $form;
  }

  /**
   *  {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->configuration['oranges_quantity'] = $form_state->getValue('oranges_quantity');
  }


}

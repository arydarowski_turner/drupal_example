<?php
/**
 * @file
 * Layout Builder admin interface.
 *
 * Contains \Drupal\layout_builder\Form\LayoutBuilder
 */

namespace Drupal\layout_builder\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

Class LayoutBuilder extends ConfigFormBase
{

  /**
   * {@inheritdoc}
   */
  public function getFormId()
  {
    return 'layout_builder_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
     return ['layout_builder.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('layout_builder.settings');

    $form['favorite_color'] = array(
      '#type' => 'textfield',
      '#title' => 'What is your favorite color?'
    );

    //dpm($form);
    //dpm($form_state);

    return $form;
  }

  /**
   * @{inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $color = array(@color => $form_state->getValue('favorite_color'));
    dsm($this->(string) t('My favorite color is @color', array('@color' => $form_state->getValue('favorite_color'))));
  }

}